const truckType = {
  _SPRINTER: 'SPRINTER',
  _SMALL_STRAIGHT: 'SMALL STRAIGHT',
  _LARGE_STRAIGHT: 'LARGE STRAIGHT',
};

const truckStatus = {
  _OL: 'OL',
  _IS: 'IS',
};

module.exports = {
  truckStatus,
  truckType,
};
