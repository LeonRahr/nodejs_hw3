const mongoose = require('mongoose');
const Joi = require('joi');

const { Schema } = mongoose;

const loadJoiShema = Joi.object({
  name: Joi.string()
    .required(),
  payload: Joi.number()
    .integer(),
  pickup_address: Joi.string(),
  delivery_address: Joi.string(),
  dimensions: Joi.object({
    width: Joi.number().integer(),
    length: Joi.number().integer(),
    height: Joi.number().integer(),
  }),
});

const Load = mongoose.model('Load', {
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  assigned_to: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  state: String,
  status: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: Number,
    length: Number,
    height: Number,
  },
  logs: [{
    message: String,
    time: Date,
  }],
  created_date: {
    type: Date,
    required: true,
  },
});

module.exports = {
  Load,
  loadJoiShema,
};
