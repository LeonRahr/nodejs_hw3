const express = require('express');

const router = express.Router();

const { shipperMiddleware } = require('../middleware/shipperMiddleware');
const { driverMiddleware } = require('../middleware/driverMiddleware');
const { paginationMiddleware } = require('../middleware/paginationMiddleware');
const {
  createLoad,
  postLoadById,
  getLoadShippingInfo,
  getLoadById,
  getActiveLoad,
  nextState,
  updateLoadById,
  deleteLoadById,
  getLoads,
} = require('../controllers/loadController');

router.get('/', paginationMiddleware, getLoads);

router.post('/', shipperMiddleware, createLoad);

router.get('/active', driverMiddleware, getActiveLoad);

router.patch('/active/state', driverMiddleware, nextState);

router.get('/:id', getLoadById);

router.put('/:id', shipperMiddleware, updateLoadById);

router.delete('/:id', shipperMiddleware, deleteLoadById);

router.post('/:id/post', shipperMiddleware, postLoadById);

router.get('/:id/shipping_info', shipperMiddleware, getLoadShippingInfo);

module.exports = {
  loadRouter: router,
};
