const { Load, loadJoiShema } = require('../models/Load');
const { Truck } = require('../models/Truck');
const { saveLoad } = require('../services/loadService');
const { canDelivered } = require('../helpers/canDelivered');
const { findQueries } = require('../helpers/findQueries');
const { loadStatus, loadState, findNextState } = require('../consts/loadConsts');
const { truckType, truckStatus } = require('../consts/truckConsts');

const getLoads = async (req, res, next) => {
  const { role, id } = req.user;
  const { status, offset, limit } = req.query;
  if (role === 'DRIVER') {
    const loads = await Load.find({ assigned_to: id }).skip(+offset).limit(+limit);
    return res.json({ loads });
  }
  const loads = await Load.find({ created_by: id }).skip(+offset).limit(+limit);
  return res.json({ loads });
};

const createLoad = async (req, res, next) => {
  const { id } = req.user;
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = req.body;

  await loadJoiShema.validateAsync({
    name, payload, pickup_address, delivery_address, dimensions,
  });

  await saveLoad(name, payload, pickup_address, delivery_address, dimensions, id);

  return res.json({ message: 'Load created successfully' });
};

const postLoadById = async (req, res, next) => {
  const { id } = req.params;
  const load = await Load.findById(id);
  load.status = loadStatus._POSTED;
  const truckTypes = canDelivered(load.dimensions, load.payload);
  const query = findQueries(truckTypes);
  let truck = null;
  if (query.length !== 0) {
		 truck = await Truck.findOne({
      $and: [
        { status: truckStatus._IS },
        { $or: query },
      ],
    });
  }
  if (!truck) {
    return res.status(400).json({ message: 'Can not find driver for you load' });
  }

  load.assigned_to = truck.assigned_to;
  load.status = loadStatus._ASSIGNED;
  load.state = loadState[0];
  await load.save();
  truck.status = truckStatus._OL;
  await truck.save();
  return res.json({ message: 'Load posted successfully', driver_found: true });
};

const getLoadShippingInfo = async (req, res, next) => {
  const { id } = req.params;
  const load = await Load.findById(id);
  const currentTruck = await Truck.findOne({ assigned_to: load.assigned_to });
  res.json({
    load,
    currentTruck,
  });
};

const getLoadById = async (req, res, next) => {
  const { id } = req.params;
  const load = await Load.findById(id);
  res.json({
    load,
  });
};

const getActiveLoad = async (req, res, next) => {
  const { id } = req.user;
  const load = await Load.findOne({
    $and: [
      { assigned_to: id },
    ],
  });
  if (!load) {
    return res.status(400).json({ message: 'User do not have active load' });
  }
  return res.json({ load });
};

const nextState = async (req, res, next) => {
  const { id } = req.user;
  const load = await Load.findOne({ assigned_to: id });
  if (!load) {
    return res.status(400).json({ message: 'User do not have active load' });
  }
  const newState = findNextState(load.state);
  if (newState !== loadState[3]) {
    load.state = newState;
    await load.save();
    return res.json({
      message: `Load state changed to '${load.state}'`,
    });
  }
  load.status = loadStatus._SHIPPED;
  load.state = newState;
  const truck = await Truck.findOne({ assigned_to: id });
  truck.status = truckStatus._IS;
  await load.save();
  await truck.save();
  return res.json({
    message: `Load state changed to '${load.state}'`,
  });
};

const updateLoadById = async (req, res, next) => {
  const { id } = req.body;
  const {
    name, payload, pickup_address, delivery_address, dimensions,
  } = req.body;
  const load = await Load.findById(id);
  if (load.status !== loadStatus._NEW) {
    return res.status(400).json({ message: 'You can change load only with status NEW' });
  }
  await Load.findByIdAndUpdate(id, {
    name, payload, pickup_address, delivery_address, dimensions,
  });
  return res.json({
    message: 'Load details changed successfully',
  });
};

const deleteLoadById = async (req, res, next) => {
  const { id } = req.body;
  const load = await Load.findById(id);
  if (load.status !== loadStatus._NEW) {
    return res.status(400).json({ message: 'You can change load only with status NEW' });
  }
  await Load.findByIdAndDelete(id);
  return res.json({ message: 'Load deleted successfully' });
};

module.exports = {
  createLoad,
  postLoadById,
  getLoadShippingInfo,
  getLoadById,
  getActiveLoad,
  nextState,
  updateLoadById,
  deleteLoadById,
  getLoads,
};
