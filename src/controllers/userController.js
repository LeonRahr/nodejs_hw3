const bcryptjs = require('bcryptjs');
const { User } = require('../models/User');

const getUserInfo = async (req, res, next) => {
  const { id } = req.user;
  const user = await User.findById(id);
  return res.json({
    user: {
      _id: user._id,
      role: user.role,
      email: user.email,
      created_date: user.created_date,
    },
  });
};

const deleteUser = async (req, res, next) => {
  const id = req.user.userId;
  await User.findByIdAndDelete(id);
  return res.json({ message: 'Profile deleted successfully' });
};

const changePassword = async (req, res, next) => {
  const id = req.user.userId;
  const password = await bcryptjs.hash(req.body.newPassword, 10);
  await User.findByIdAndUpdate(id, { password });
  return res.json({ message: 'Password changed successfully' });
};

module.exports = {
  getUserInfo,
  deleteUser,
  changePassword,
};
