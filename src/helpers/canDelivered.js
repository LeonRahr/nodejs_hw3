const isSprinter = (dimensions, payload) => {
  const width = 300;
  const length = 250;
  const height = 170;
  const payloadCar = 1700;
  return dimensions.width <= width
		&& dimensions.length <= length
		&& dimensions.height <= height
		&& payload <= payloadCar;
};

const isSmall = (dimensions, payload) => {
  const width = 500;
  const length = 250;
  const height = 170;
  const payloadCar = 2500;
  return dimensions.width <= width
		&& dimensions.length <= length
		&& dimensions.height <= height
		&& payload <= payloadCar;
};

const isLarge = (dimensions, payload) => {
  const width = 700;
  const length = 350;
  const height = 200;
  const payloadCar = 4000;
  return dimensions.width <= width
		&& dimensions.length <= length
		&& dimensions.height <= height
		&& payload <= payloadCar;
};

const canDelivered = (dimensions, payload) => {
  const truckTypes = [];
  if (isSprinter(dimensions, payload)) {
    truckTypes.push('SPRINTER');
  }
  if (isSmall(dimensions, payload)) {
    truckTypes.push('SMALL STRAIGHT');
  }
  if (isLarge(dimensions, payload)) {
    truckTypes.push('LARGE STRAIGHT');
  }
  return truckTypes;
};

module.exports = {
  canDelivered,
};
