const { Truck } = require('../models/Truck');

const saveTruck = async (type, userId) => {
  const newTruck = new Truck({
    type,
    created_by: userId,
    status: 'IS',
    created_date: new Date(),
  });

  await newTruck.save();
};

module.exports = {
  saveTruck,
};
