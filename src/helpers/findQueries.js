const findQueries = (truckTypes) => {
  const queries = [];
  truckTypes.forEach((type) => {
    queries.push({ type });
  });
  return queries;
};

module.exports = {
  findQueries,
};
