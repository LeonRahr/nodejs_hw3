const express = require('express');

const router = express.Router();

const { getUserInfo, changePassword, deleteUser } = require('../controllers/userController');

router.get('/', getUserInfo);

router.delete('/', deleteUser);

router.patch('/password', changePassword);

module.exports = {
  userRouter: router,
};
