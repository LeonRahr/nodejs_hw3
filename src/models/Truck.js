const mongoose = require('mongoose');
const Joi = require('joi');

const { Schema } = mongoose;

const truckJoiSchema = Joi.object({
  type: Joi.string()
    .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
    .uppercase()
    .required(),
});

const Truck = mongoose.model('Truck', {
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  assigned_to: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
  },
  created_date: {
    type: Date,
    required: true,
  },
});

module.exports = {
  truckJoiSchema,
  Truck,
};
