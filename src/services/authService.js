const bcryptjs = require('bcryptjs');
const { User } = require('../models/User');

const saveUser = async ({ role, email, password }) => {
  const user = new User({
    role,
    email,
    password: await bcryptjs.hash(password, 10),
    created_date: new Date(),
  });

  await user.save();
};

module.exports = {
  saveUser,
};
