# Node_js hw3

It's homework project for EMAP laba spring.

## Usage

```python
git pull

npm install

npm start
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please check openapi.yaml for learn API.

## License
(https://gitlab.com/LeonRahr/)