const express = require('express');

const router = express.Router();

const {
  getTrucks,
  assignTruckToUser,
  deleteTruckById,
  updateTruckById,
  getTruckById,
  createTruck,
} = require('../controllers/TruckController');

router.get('/', getTrucks);

router.post('/', createTruck);

router.get('/:id', getTruckById);

router.put('/:id', updateTruckById);

router.delete('/:id', deleteTruckById);

router.post('/:id/assign', assignTruckToUser);

module.exports = {
  truckRouter: router,
};
