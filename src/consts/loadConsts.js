const loadStatus = {
  _NEW: 'NEW',
  _POSTED: 'POSTED',
  _ASSIGNED: 'ASSIGNED',
  _SHIPPED: 'SHIPPED',
};

const loadState = ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'];

const findNextState = (currentState) => {
  const index = loadState.findIndex((state) => state === currentState);
  return loadState[index + 1];
};

module.exports = {
  loadState,
  loadStatus,
  findNextState,
};
