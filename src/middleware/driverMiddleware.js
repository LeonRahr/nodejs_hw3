const driverMiddleware = (req, res, next) => {
  const { role } = req.user;
  if (role !== 'DRIVER') {
    return res.status(400).json({ message: 'available only for DRIVER role' });
  }
  return next();
};

module.exports = {
  driverMiddleware,
};
