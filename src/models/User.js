const mongoose = require('mongoose');
const Joi = require('joi');

const userJoiSchema = Joi.object({
  role: Joi.string()
    .valid('SHIPPER', 'DRIVER')
    .uppercase()
    .required(),
  email: Joi.string()
    .email()
    .lowercase()
    .required(),
  password: Joi
    .string()
    .alphanum()
    .min(4)
    .max(10),
});

const User = mongoose.model('User', {
  role: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  created_date: {
    type: Date,
    required: true,
  },
});

module.exports = {
  User,
  userJoiSchema,
};
