const express = require('express');

const router = express.Router();
const { registerUser, loginUser } = require('../controllers/authController');

const asyncWrapper = (controler) => (req, res, next) => controler(req, res, next).catch(next);

router.post('/register', asyncWrapper(registerUser));

router.post('/login', loginUser);

module.exports = {
  authRouter: router,
};
