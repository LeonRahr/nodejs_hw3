const express = require('express');
const morgan = require('morgan');

const app = express();
const port = process.env.NODE_ENV || 8080;
const mongoose = require('mongoose');

const { authMiddleware } = require('./middleware/authMiddleware');

const { authRouter } = require('./routers/authRouter');
const { userRouter } = require('./routers/userRouter');
const { truckRouter } = require('./routers/truckRouter');
const { loadRouter } = require('./routers/loadRouter');

function errorHandler(err, req, res, next) {
  res.status(500).send({ message: err.message });
}

mongoose.connect('mongodb+srv://admin:admin@cluster0.zxsrpwe.mongodb.net/?retryWrites=true&w=majority');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', authMiddleware, userRouter);
app.use('/api/trucks', authMiddleware, truckRouter);
app.use('/api/loads', authMiddleware, loadRouter);

const start = async () => {
  try {
    app.listen(port);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

app.use(errorHandler);
