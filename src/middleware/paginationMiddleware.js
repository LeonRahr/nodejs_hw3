const paginationMiddleware = (req, res, next) => {
  if (!req.query.limit) {
    req.query.limit = 10;
  }
  if (+req.query.limit > 50) {
    req.query.limit = 50;
  }
  if (!req.query.offset) {
    req.query.offset = 0;
  }
  next();
};

module.exports = {
  paginationMiddleware,
};
