const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User, userJoiSchema } = require('../models/User');
const { saveUser } = require('../services/authService');

const registerUser = async (req, res, next) => {
  const { email, password, role } = req.body;
  await userJoiSchema.validateAsync({ email, password, role });

  await saveUser({ email, password, role });
  return res.json({ message: 'Profile created successfully' });
};

const loginUser = async (req, res, next) => {
  console.log(req.body);
  const user = await User.findOne({ email: req.body.email });
  console.log(user);
  if (user && await bcryptjs.compare(String(req.body.password), String(user.password))) {
    const payload = { email: user.email, role: user.role, userId: user._id };
    const jwtToken = jwt.sign(payload, 'secret-key');
    return res.json({ jwt_token: jwtToken });
  }
  return res.status(403).json({ message: 'Not found' });
};

module.exports = {
  registerUser,
  loginUser,
};
