const shipperMiddleware = (req, res, next) => {
  const { role } = req.user;
  console.log(role);
  if (role !== 'SHIPPER') {
    return res.status(400).json({ message: 'available only for SHIPPER role' });
  }

  return next();
};

module.exports = {
  shipperMiddleware,
};
