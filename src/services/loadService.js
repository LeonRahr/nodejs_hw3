const { Load } = require('../models/Load');

const saveLoad = async (
  name,
  payload,
  pickup_address,
  delivery_address,
  dimensions,
  id,
) => {
  const newLoad = new Load({
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
    status: 'NEW',
    created_date: new Date(),
    created_by: id,
  });

  await newLoad.save();
};

module.exports = {
  saveLoad,
};
