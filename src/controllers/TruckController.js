const { Truck, truckJoiSchema } = require('../models/Truck');
const { saveTruck } = require('../services/truckService');

const getTrucks = async (req, res, next) => {
  const { id } = req.user;
  const trucks = await Truck.find({ created_by: id });

  res.json({
    trucks: [...trucks],
  });
};

const createTruck = async (req, res, next) => {
  const { id } = req.user;
  const { type } = req.body;
  console.log(req.user);
  await truckJoiSchema.validateAsync({ type });

  await saveTruck(type, id);

  return res.json({ message: 'Truck created successfully' });
};

const getTruckById = async (req, res, next) => {
  const { id } = req.params;
  const truck = await Truck.findById(id);

  res.json({
    truck,
  });
};

const updateTruckById = async (req, res, next) => {
  const { id } = req.params.id;
  const { type } = req.body;

  await Truck.findByIdAndUpdate(id, { type });

  res.json({
    message: 'Truck details changed successfully',
  });
};

const deleteTruckById = async (req, res, next) => {
  const { id } = req.params.id;

  await Truck.findByIdAndDelete(id);

  res.json({
    message: 'Truck deleted successfully',
  });
};

const assignTruckToUser = async (req, res, next) => {
  const { id } = req.params;
  const { id: userId } = req.user;
  await Truck.findByIdAndUpdate(id, { assigned_to: userId });

  res.json({
    message: 'Truck assigned successfully',
  });
};

module.exports = {
  getTrucks,
  createTruck,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckToUser,
};
